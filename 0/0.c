#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>


int start_index(int arSize, int thread_num, int thread_count){
	int pack_size = arSize / thread_count;
	int mod = arSize % thread_count;
	if(thread_num < mod){
		return (pack_size + 1) * thread_num;
	}	
	else{
		return (pack_size + 1) * mod + (thread_num - mod) * pack_size;
	}
}

int pack_size(int arSize, int thread_num, int thread_count){
	int pack_size = arSize / thread_count;
        int mod = arSize % thread_count;
	if(thread_num < mod){
		return pack_size + 1;
	}	
	else{
		return pack_size;
	}
}


int main(int argc, char *argv[]){
	MPI_Status Status;
	MPI_Init(&argc, &argv);
	
	int arSize = atoi(argv[1]);

	int myrank, size;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

	if(myrank == 0){
		int a[arSize];
		for(int i = 0; i < arSize; i++){
			a[i] = i;
		}

		int target = 0;
		for(int thread_num = 1; thread_num < size; thread_num++){
			int start = start_index(arSize, thread_num, size);
			int pack = pack_size(arSize, thread_num, size);
			MPI_Send(&a[start], pack, MPI_INT, thread_num, thread_num, MPI_COMM_WORLD);
		}

		int common_summ = 0;

		for(int source = 1; source < size; source++){
			int temp;
			MPI_Recv(&temp, 1, MPI_INT, source, 0, MPI_COMM_WORLD, &Status);
			common_summ += temp;
		}
		
		for(int index = 0; index < pack_size(arSize, 0, size); index++){
			common_summ += a[index];
		}

		int main_summ = 0;
		for(int i = 0; i < arSize; i++){
			main_summ += a[i];
		}

		printf("Got sum %d from threads\n", common_summ);
		printf("Counted sum %d by myself\n", main_summ);

	}
	else{
		int pack = pack_size(arSize, myrank, size);
		int recieved[pack];
		MPI_Recv(&recieved[0], pack, MPI_INT, 0, myrank, MPI_COMM_WORLD, &Status);
		int sum = 0;
		for(int i = 0; i < pack; i++){
			sum += recieved[i];
		}
		printf("Im thread %d sum %d\n", myrank, sum);

		MPI_Send(&sum, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);

	}


	MPI_Finalize();
	return 0;
}

