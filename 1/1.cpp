#include <iostream>
#include <mpi.h>
#include <vector>
#include <cmath>
#include <ctime>

using namespace std;

unsigned int start_time = clock();

const double finalTime = 0.1;
const double h = 0.002;
const double k = 1;
const double dt = 0.0002;
const double l = 1;
const double u0 = 1;
const double borderConditions = 0;

size_t getLen(double x) {
    return static_cast<size_t> (x / h);
}

size_t getTime(double t) {
    return static_cast<size_t> (t / dt);
}

size_t getInd(size_t rank, size_t count) {
    if(rank == 0){
    	return 0;
    }
    size_t size = getLen(l) - 1;
    size_t step = size / count;
    size_t mod = size % count;
    size_t ind = step * rank + min(rank, mod);
    return ind;
}

double getPart(size_t m, double x, double t) {
    size_t par = 2 * m + 1;
    return exp(-k * pow(M_PI * par / l, 2) * t) * sin(M_PI * x * par / l) / par;
}

double getExactSolution(size_t i, size_t s) {
    const double x = i * h;
    const double t = s * dt;
    const double coef = 4 * u0 / M_PI;

    size_t m = 0;
    double result = 0;
    double part = getPart(m, x, t);
    while (fabs(part) > 1e-15) {
        ++m;
        result += part;
        part = getPart(m, x, t);
    }
    return result * coef;
}

int main(int argc, char* argv[]) {
    const double coef = dt * k / (h * h);

    // initialisation
    MPI_Init(&argc, &argv);
    MPI_Status Status;
    MPI_Request Request;
    int myrank;
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

    auto t_0 = MPI_Wtime();

    if (myrank == 0) {
	size_t length = static_cast<size_t>(l / h);
	vector<double> approx(length);
	vector<double> exact(length);

	//count exact
	//recieve approx
	for(size_t i = 1; i < size; i++){
		size_t index = getInd(i - 1, size - 1);
		size_t length = getInd(i, size - 1) - getInd(i - 1, size - 1);
		MPI_Recv(&approx[index], length, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}

	//output
	cout << 0 << " ";
	size_t step = length / 10;
	for(size_t i = step; i < length; i += step){
		cout << approx[i] << " ";
	}
	cout << 0 << endl;

	//time
	auto t = MPI_Wtime();
	cout << "time: " << t - t_0 << endl;
	
    } else {
	vector<double> my_points(getInd(myrank, size - 1) - getInd(myrank - 1, size - 1) + 2, 1);
	size_t my_length = my_points.size();
	if(myrank == 1){
	    my_points[0] = borderConditions;
	}
	if(myrank == size - 1){
	    my_points[my_length - 1] = borderConditions;
	}

	for(double time = 0; time < finalTime; time += dt){
	    //recount
	    for(size_t index = 1; index < my_length - 1; index++){
	    	double u_i = my_points[index];
		double u_prev = my_points[index - 1];
		double u_next = my_points[index + 1];

		my_points[index] = u_i + coef * (u_next - 2 * u_i + u_prev);
	    }
	
	    if(myrank > 1){
	        MPI_Send(&my_points[1], 1, MPI_DOUBLE, myrank - 1, 0, MPI_COMM_WORLD);
	    }
	    if(myrank < size - 1){
	        MPI_Send(&my_points[my_length - 2], 1, MPI_DOUBLE, myrank + 1, 0, MPI_COMM_WORLD);
	    }
	    if(myrank > 1){
                MPI_Recv(&my_points[0], 1, MPI_DOUBLE, myrank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
            if(myrank < size - 1){
                MPI_Recv(&my_points[my_length - 1], 1, MPI_DOUBLE, myrank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            }
	}

	MPI_Send(&my_points[1], my_length - 2, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();

    return 0;
}

